import java.util.*;

// https://www.spoj.com/problems/TESSER/
// Hmmmmmmmmm...do you know why I'm getting Wrong Answer?

public class FindTheBug11 {
    // Compute the Longest Prefix Suffix (LPS) array for a string
    public static int[] lpsTable(String toSearch) {
        int[] table = new int[toSearch.length()];
        int prefixInd = 0;
        /* For each index i, lps[i] should equal the length of the
           longest prefix of toSearch which is also a suffix (substring)
           ending at index i   */
        for (int i = 1; i < table.length; i++) {
            if (toSearch.charAt(i) == toSearch.charAt(prefixInd)) {
                prefixInd++;
                table[i] = prefixInd;
            } else {
                prefixInd = 0;
            }
        }
        return table;
    }

    // Run Knuth-Morris-Pratt to determine whether or not the pattern string is
    // contained in toSearch
    public static boolean containsPatternKMP(String toSearch, String pattern) {
        // Prepare the LPS table
        int[] lps = lpsTable(toSearch);
        int patternInd = 0;
        boolean foundPattern = false;
        for (int i=0; i<toSearch.length(); i++) {
            if (toSearch.charAt(i) == pattern.charAt(patternInd)) {
                // Characters match, advance pattern pointer
                patternInd++;
            } else {
                // Mismatch
                if (patternInd > 0) {
                    // If progress has been made, revert back to
                    // part of string that needs to be re-checked
                    patternInd = lps[patternInd - 1];
                    i--;
                }
            }
            if (patternInd == pattern.length()) {
                // Pattern occurence was found
                foundPattern = true;
                break;
            }
        }
        return foundPattern;
    }

    // Translate hill heights into a message string
    public static String getComparisons(int[] heights) {
        StringBuilder comparisons = new StringBuilder();
        for (int i=0; i<heights.length-1; i++) {
            if (heights[i] < heights[i+1]) {
                comparisons.append('G');
            } else if (heights[i] > heights[i+1]) {
                comparisons.append('L');
            } else {
                comparisons.append('E');
            }
        }
        return comparisons.toString();
    }

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int numCases = in.nextInt();
        for (int t=0; t<numCases; t++) {
            // Parse heights
            int numHills = in.nextInt();
            int[] heights = new int[numHills];
            for (int i=0; i<numHills; i++) {
                heights[i] = in.nextInt();
            }
            // Clear Scanner buffer
            in.nextLine();
            // Parse message
            String message = in.nextLine();
            String comparisonString = getComparisons(heights);
            // Solve
            boolean answer = containsPatternKMP(comparisonString, message);
            if (answer) {
                System.out.println("YES");
            } else {
                System.out.println("NO");
            }
        }
    }
}
